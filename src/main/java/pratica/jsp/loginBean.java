/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pratica.jsp;

/**
 *
 * @author Matsunaga
 */
public class loginBean {
    private String login = "";
    private String senha = "";
    private int perfil;
    private String nomePerfil = "";

    /**
     * Constructor
     */
    public loginBean() {
    }

    /**
     * All Get and Set methods
     * @return 
     */
    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public int getPerfil() {  
        return perfil;
    }

    public void setPerfil(int perfil) {
        this.perfil = perfil; 
    }
    
    public String getnomePerfil() {  
        
        if(perfil == 1){
            nomePerfil = "Cliente";
        }else if(perfil == 2){
            nomePerfil = "Gerente";
        }else{
            nomePerfil = "Administrador";
        }
        return nomePerfil;
    }

    public void setnomePerfil(String nomePerfil) {
        this.nomePerfil = nomePerfil; 
    }
}





