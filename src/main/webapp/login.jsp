<%-- 
    Document   : index
    Created on : 28/05/2015, 18:22:02
    Author     : Matsunaga
--%>

<%@page import="pratica.jsp.loginBean"%>
<%@page import="java.util.Date"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean class="pratica.jsp.loginBean" id="autenticacao" scope="session"/>
<jsp:setProperty name="autenticacao" property="*" />

<!DOCTYPE html>
<html>
   <head>
      <title>Login</title>
   </head>
   <body>
      <form method='post' action="/pratica-jsp/login.jsp">
            Código: <input type='text' name='login' value='${autenticacao.login}'/><br/>
            Nome: <input type='password' name='senha' value='${autenticacao.senha}'/><br/>
            Perfil: <select name='perfil'>
                     <option value='1'>Cliente</option>
                     <option value='2'>Gerente</option>
                     <option value='3'>Administrador</option>
                    <select>
            <input type='submit' value='Enviar'/>
      </form>          
   </body>
</html>
            
<%if (request.getMethod().equals("POST")) {%>
    <%if(autenticacao.getLogin().equals(autenticacao.getSenha())){%> 
        <div>
            <h1 style='color:blue'><i>${autenticacao.nomePerfil}, login bem sucedido, para ${autenticacao.login} às <%= new java.util.Date() %><i></h1>
        </div> 
    <%}else{%> 
        <div>
            <h1 style='color:red'><i>Acesso negado<i></h1>
        </div>  
    <%}%>
<%}%> 
